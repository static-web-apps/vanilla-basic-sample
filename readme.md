# **GitLab Pipelines Sample: Azure Static Web Apps Deploy**

This sample uses the GitLab(static-web-apps) Azure Web Apps Deploy to deploy a Vanilla Basic app to [App Service Static Web Apps](https://docs.microsoft.com/en-us/azure/static-web-apps/).

This repo is used as a starter for a very basic HTML web application using no front-end frameworks.

**Prerequisites** 
- Active Azure account: If you don't have one, you can [create an account for free](https://azure.microsoft.com/en-us/free/).
- GitLab project: If you don't have one, you can [create a project for free](https://docs.gitlab.com/ee/user/project/working_with_projects.html#create-a-project).
- GitLab includes Pipelines. If you need help getting started with Pipelines, see [create your first pipeline](https://docs.gitlab.com/ee/ci/quick_start/).

**Support**

This sample is provided "as is" and is not supported. Likewise, no commitments are made as to its longevity or maintenance.
